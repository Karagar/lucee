<cfcomponent>
    <cffunction
        name="getResponse"
        access="remote"
        returntype="struct"
        output="false">
        <CFSET Request.datasource = "masterdata">
        <cfargument name="manager" type="string" required="false" default="" />
        <cfargument name="sort_column" type="string" required="false" default="" />
        <cfargument name="sort_order" type="string" required="false" default="" />
        <cfparam name="url.sort_column" type="string" default="#sort_column#">
        <cfparam name="url.sort_order" type="string" default="#sort_order#">
        <cfif listfindnocase("1,2,3,4", url.sort_column)>
            <cfset safeSortColumn = url.sort_column>
        </cfif>
        <cfif listfindnocase("DESC,ASC", url.sort_order)>
            <cfset safeSortOrder = url.sort_order>
        </cfif>
        <cfquery name="qry" datasource=#Request.datasource#>
            select customer_id, customer, service_manager, contragents from v_customer 
            <cfif ( #manager# != 0 )> where service_manager_id = <cfqueryparam value = #manager#> </cfif>
            <cfif ( #safeSortColumn# != "" )> order by #safeSortColumn# #safeSortOrder#</cfif>;
        </cfquery>
        <cfset var LOCAL = {}/>
        <cfset LOCAL.Response = 
        {
            Success = true,
            Error = "",
            Data = SerializeJSON(#qry#)
        }/>
        <cfreturn LOCAL.Response />
    </cffunction>
</cfcomponent>
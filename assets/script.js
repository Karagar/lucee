onLoad = function ()
{
	lastOrder = "";
	this.form = $( "#select_form" );
	this.form.submit
    (
    	function()
    	{
    		SubmitForm();
    		return(false);
    	}
    );
    SubmitForm();
}

SubmitForm = function(sortColumnArg)
{
	if ((sortColumnArg != undefined)&&(sortColumnArg == lastOrder)) sortOrderArg = "DESC"; 
	else if ((sortColumnArg != undefined)&&(sortColumnArg != lastOrder)) sortOrderArg = "ASC";
	if (sortColumnArg == undefined) sortColumnArg = 1;
	if (typeof sortOrderArg == "undefined") sortOrderArg = "ASC";
    lastOrder = sortColumnArg;
	this.form = $( "#select_form" );
	this.table = $( "#main_table" );
	this.References = 
    {
        Name: this.form.find( "input[ name = 'selected_manager' ]" ),
        Template: this.table.find( "div.template" ),
        Content: this.table.find( "div.content" )
    };
	if (( js_service_array.id[js_service_array.name.indexOf(this.References.Name.val())] == undefined ) && ( this.References.Name.val() != "" ))
	{
		arg = -1;
	}
	else
	{
		arg = 0;
	}
	if ( js_service_array.id[js_service_array.name.indexOf(this.References.Name.val())] != undefined )
	{
		arg = js_service_array.id[js_service_array.name.indexOf(this.References.Name.val())];
	}
	if (arg >= 0)
	{
		$.ajax(
	        {
	            type: "get",
	            url: "./assets/api.cfc",
	            data: 
	            {
	                method: "getResponse",
	                manager: arg,
	                sort_column: lastOrder,
	                sort_order: sortOrderArg
	            },
	            dataType: "json",
	            success: function(objResponse){
	                if (objResponse.SUCCESS){
	                	drow( JSON.parse(objResponse.DATA).DATA );
	                } else {
	                    console.log( objResponse.ERROR );
	                }
	            },
	            error: function( objRequest, strError ){
	                console.log( "Неизвестная ошибка: " + strError );
	            }
	        }
	    );
	}
	else
	{
		drow([]);
	}
    
    drow = function( data )
    {
    	this.References.Content.empty();
    	data.forEach( function callback(currentValue, index, array) 
    	{
    		row = this.References.Template.children().clone();
        	row.find( "div.id" ).text( currentValue[0] );
        	row.find( "div.customer" ).text( currentValue[1] );
        	row.find( "div.manager" ).text( currentValue[2] );
        	row.find( "div.contragents" ).text( currentValue[3] );
        	this.References.Content.append( row );
		});
    }
};

window.onload = onLoad();